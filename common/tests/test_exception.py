from scraper.exceptions import ScraperError

import unittest

def a_dummy_function():
    """" Test for sending Exception""""
    raise ScraperError("raising an error")

class TestProxyRotator(unittest.TestCase):

    def test_scraper_error(self):
        """""Test is the Scraper can return an exception""""
        self.assertRaises(ScraperError, a_dummy_function)


if __name__ == '__main__':
    unittest.main()
