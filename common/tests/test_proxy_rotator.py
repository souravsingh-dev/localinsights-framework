from scraper.proxy_rotator import ProxyRotator

import unittest


class TestProxyRotator(unittest.TestCase):
    """"" Unitest class to check for Proxy Rotator"""" 
    
    def test_get_proxy(self):
    """" Test to check for valid proxy location""""
        pr = ProxyRotator()
        res = pr.proxies
        self.assertIn("http://locinsights",
                      res["http"],
                      "http gives a web url")
        self.assertIn("http://locinsights",
                      res["https"],
                      "https has a valid proxy")


if __name__ == "__main__":
    unittest.main()
